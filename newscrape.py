#!/usr/bin/env python

import sys
import os
import urllib.request
from bs4 import BeautifulSoup

if len(sys.argv) == 1:
    print("\nUsage: newscrape.py subreddit")
    sys.exit()
elif len(sys.argv) == 2:
    subreddit = sys.argv[1]

url = "https://old.reddit.com/r/" + subreddit + "/new/"

#download the URL and extract the content to the variable html
request = urllib.request.Request(url, headers={'User-Agent': 'Mozilla/5.0'})
html = urllib.request.urlopen(request).read()

#pass the html to Beautifulsoup
soup = BeautifulSoup(html, 'html.parser')

main_table = soup.find("div", attrs={'id':'siteTable'})

links = main_table.find_all("a", class_ = "title")

extracted_records = []

for link in links:

    title = link.text
    url = link['href']

    if not url.startswith('http'):
        url = "https://old.reddit.com" + url

    print("\n%s : %s"%(title, url))
    print("\n-------------------------------------------------------------------------------------------------------------------------------------------------------")

    record = url

    extracted_records.append(record)

outfile = subreddit + ".txt"


file = open(outfile, mode='w+', encoding='utf-8')
file.write('\n'.join(extracted_records))

#Download Image code

with open(outfile, 'r') as file :
  filedata = file.read()

# Replace the target string
filedata = filedata.replace(',', '')
filedata = filedata.replace('"', '')
filedata = filedata.replace('[', '')
filedata = filedata.replace(']', '')
filedata = filedata.replace(' ', '')

# Write the file out again
with open(outfile, 'w') as file:
  file.write(filedata)

# Python3 code to iterate over a list
list = []
fh = open(outfile)
for line in fh:
    # in python 2
    # print line
    # in python 3
    list.append(line.rstrip('\n'))

length = len(list)

for i in range(length):
    link =list[i]
    myCmd = 'you-get -o'+subreddit+' '+link
    os.system(myCmd)
