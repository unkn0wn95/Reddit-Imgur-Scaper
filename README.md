
# Reddit Imgur Scraper

This script goes to a new or linked subreddit and download images of the post.

## INSTALLATION
```
  pip install you-get
  git clone or download the zip file
```

## HOW TO USE

### Download by subreddit
```
python3 newscrape.py subreddit
```
  
### Download by link
-Get Link at old.reddit.com only
-paste the link in the file
```
  python3 linkscrape.py
```
